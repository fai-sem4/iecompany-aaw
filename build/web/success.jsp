<%-- 
    Document   : success.jsp
    Created on : Jun 27, 2019, 2:10:20 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <center>
            <h1>Survey Infomation</h1>
            <b>Age: </b><s:property value="age"/><br>
            <b>Product: </b><s:property value="product"/><br>
            <b>Country: </b><s:property value="country"/><br>
            <b>Gender: </b><s:property value="gender"/><br>
        </center>
    </body>
</html>
